package com.example.demo_firebase.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.demo_firebase.R;
import com.example.demo_firebase.models.Manga;

public class AddMangaActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_manga_activity);

        Manga manga = (Manga) getIntent().getSerializableExtra("Manga");


        TextView tvMangaName = findViewById(R.id.TVMangaName);
        EditText etMangaName = findViewById(R.id.editTextMangaName);
        EditText etMangaCategory = findViewById(R.id.editTextCategory);
        RatingBar ratingBar = findViewById(R.id.rating);

        boolean addNewManga = true;

        Button addButton = findViewById(R.id.addButton);

        if (manga == null){
            System.out.println(manga);
        }else {
            tvMangaName.setText(manga.getName());
            etMangaName.setText(manga.getName());
            etMangaCategory.setText(manga.getCategory());
            ratingBar.setRating(manga.getRate());

            addNewManga = false;

        }


        boolean finalAddNewManga = addNewManga;
        addButton.setOnClickListener(v -> {
            if (etMangaName.getText().toString().isEmpty()){
                Toast.makeText(getApplicationContext(), "Es necesario rellenar todos los campos", Toast.LENGTH_SHORT).show();
            }else{
                if (etMangaCategory.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Es necesario rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }else{
                    String mangaName =etMangaName.getText().toString();
                    String mangaCategory = etMangaCategory.getText().toString();
                    float rate = ratingBar.getRating();

                    if (finalAddNewManga){

                        String key = MainActivity.reference.push().getKey();

                        Manga newManga = new Manga(key, mangaName, mangaCategory, rate);
                        newManga.setMangaId(key);
                        assert key != null;
                        MainActivity.reference.child(key).setValue(newManga);
                    }else{
                        manga.setMangaId(manga.getMangaId());

                        MainActivity.reference.child(manga.getMangaId()).setValue(
                                new Manga(manga.getMangaId(),mangaName, mangaCategory, rate)
                        );
                    }

                finish();

                }
            }
        });

    }
}
