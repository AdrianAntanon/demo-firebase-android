package com.example.demo_firebase.models;

import java.io.Serializable;

public class Manga implements Serializable {
    private String name, category, mangaId;
    private float rate;

    public Manga(){

    }

    public Manga(String mangaId, String name, String category,  float rate) {
        this.name = name;
        this.category = category;
        this.mangaId = mangaId;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMangaId() {
        return mangaId;
    }

    public void setMangaId(String mangaId) {
        this.mangaId = mangaId;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
}
