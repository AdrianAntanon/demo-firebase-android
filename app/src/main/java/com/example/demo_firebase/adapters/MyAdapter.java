package com.example.demo_firebase.adapters;

import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demo_firebase.R;
import com.example.demo_firebase.activities.AddMangaActivity;
import com.example.demo_firebase.models.Manga;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MyAdapter extends FirebaseRecyclerAdapter<Manga, MyAdapter.MangaHolder> {

    public MyAdapter(@NonNull FirebaseRecyclerOptions<Manga> options) {
        super(options);
    }

    @Override
    public void onBindViewHolder(@NonNull MangaHolder holder, int position, @NonNull Manga manga) {
        holder.bind(manga);
    }

    @NonNull
    @Override
    public MangaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new MangaHolder(view);
    }

    public void deleteItem(int position) {
        Manga manga = getItem(position);
        DatabaseReference reference;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database.getReference("Manga");
        reference.child(manga.getMangaId()).removeValue();
    }

    public static class MangaHolder extends RecyclerView.ViewHolder{
        TextView tvName;
        TextView tvCategory;
        TextView tvRate;

        public MangaHolder(@NonNull View itemView) {
            super(itemView);
            this.tvName = itemView.findViewById(R.id.textViewName);
            this.tvCategory = itemView.findViewById(R.id.textViewCategory);
            this.tvRate = itemView.findViewById(R.id.textViewRate);
        }

        public void bind(Manga manga){
            tvName.setText(manga.getName());
            tvCategory.setText(manga.getCategory());
            String manga_rate = manga.getRate()+"";
            tvRate.setText(manga_rate);

            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(itemView.getContext(), AddMangaActivity.class);
                intent.putExtra("Manga", manga);
                itemView.getContext().startActivity(intent);
            });

        }

    }

    public interface OnMangaListener{
        void onMangaClick(View view ,int position);
        void onItemLongClick(View view, int position);
    }
}
